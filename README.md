"SerializableEnum" code repository
==================================

SerializableEnum
----------------

This repository contains the "SerializableEnum" class, a C++
templated class that allows to use a low-level C enum with the
convenience of a C# enum - e.g. a toString() method, an iterator
interface over all possible values, convenience class methods etc.

It should work with C++11 or higher, possibly also older versions.

See Common/Support/SerializableEnum.ut.cpp for an example on how to use
this class template. Pay attention to the internal helper struct
`ExampleEnum\_struct`, and how it is used in the template's subclass
`ExampleEnum`.

This template code can optionally be linked against [libvariant][lvar].
For using this, declare the preprocessor macro

```
HAVE_LIBVARIANT
```

before including.

This template was developed by Philipp Kaluza, with much-appreciated
insightful input by Daniel Beck.

This work was sponsored by Cognotekt GmbH, but is no longer maintained
by them.

[lvar]: https://gitlab.com/ghostroute/libvariant/

SerializableMap
---------------
FIXME: document how this works and how to use it. Possibly make it work
without {From,To}VariantAspect. Add a test case ?


License
-------

Copyright 2016 Cognotekt GmbH

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

License details are in the file COPYING.AL20.
