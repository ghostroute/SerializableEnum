///////////////////////////////////////////////////////////
// SerializableEnum.ut.cpp
//
// (C) 2016 Cognotekt GmbH
// Authors: Philipp Kaluza <philipp.kaluza@cognotekt.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created by pkaluza on 2016-04-18
//
///////////////////////////////////////////////////////////

#define BOOST_TEST_MODULE SerializableEnum

#include "SerializableEnum.hpp"
#include <boost/test/unit_test.hpp>
#include <iostream>

using namespace boost::unit_test;

#define EXAMPLEENUM_ALLVALUES \
    ENUM_VALUE(Example1) \
    ENUM_VALUE(Example2) \
    ENUM_VALUE(EXAMPLE3)

struct ExampleEnum_struct {
    enum _raw {
        #define ENUM_VALUE(x) x,
        EXAMPLEENUM_ALLVALUES
        #undef ENUM_VALUE
    };

    static void register_enums(cognotekt::Common::register_enum_func f) {
        #define ENUM_VALUE(x) f ((int)x, #x);
        EXAMPLEENUM_ALLVALUES
        #undef ENUM_VALUE
    }
};

class ExampleEnum: public cognotekt::Common::SerializableEnum<ExampleEnum_struct> {
public:
    ExampleEnum(RawEnum val) :
            SE_type(val) { }

    explicit ExampleEnum(const std::string& var) :
            SE_type(var) { }

#ifdef HAVE_LIBVARIANT
    explicit ExampleEnum(libvariant::Variant var) :
            SE_type(var) { }
#endif
};

struct UnitTestsConfig
{
    UnitTestsConfig()
    {
        boost::unit_test::unit_test_log.set_threshold_level(log_successful_tests);
    }
};

BOOST_GLOBAL_FIXTURE(UnitTestsConfig);

BOOST_AUTO_TEST_SUITE(UnitTestSE)

BOOST_AUTO_TEST_CASE(SerializableEnumInited) {
    BOOST_CHECK_EQUAL(false, ExampleEnum::initialized());

    ExampleEnum a(ExampleEnum::Example1);
    std::string str = a.toString();

    BOOST_CHECK_EQUAL(true, ExampleEnum::initialized());
}

BOOST_AUTO_TEST_CASE(SerializableEnumToString) {
    ExampleEnum a(ExampleEnum::Example2);
    std::string str(a.toString());

    BOOST_CHECK_EQUAL(str, "Example2");
}

BOOST_AUTO_TEST_CASE(SerializableEnumFromString)
{
    std::string str("Example1");
    ExampleEnum ct = ExampleEnum::fromString(str);

    BOOST_CHECK_EQUAL((int)ct.getValue(), (int)ExampleEnum::Example1);
}

#ifdef HAVE_LIBVARIANT
BOOST_AUTO_TEST_CASE(SerializableEnumToVariant)
{
    ExampleEnum e1(ExampleEnum::Example1);
    BOOST_CHECK_EQUAL(e1.getVariant().AsString(),"Example1");

    ExampleEnum e2(ExampleEnum::Example2);
    BOOST_CHECK_EQUAL(e2.getVariant().AsString(),"Example2");

    ExampleEnum e3(ExampleEnum::EXAMPLE3);
    BOOST_CHECK_EQUAL(e3.getVariant().AsString(),"EXAMPLE3");
}

BOOST_AUTO_TEST_CASE(SerializableEnumFromVariant)
{
    libvariant::Variant v1("Example1");
    ExampleEnum e1(v1);
    BOOST_CHECK_EQUAL((int)e1.getValue(), (int)ExampleEnum::Example1);
    
    libvariant::Variant v2("Example2");
    ExampleEnum e2(v2);
    BOOST_CHECK(e2.getValue() == ExampleEnum::Example2);
    
    libvariant::Variant v3("EXAMPLE3");
    ExampleEnum e3(v3);
    BOOST_CHECK(e3.getValue() == ExampleEnum::EXAMPLE3);
}
#endif

BOOST_AUTO_TEST_CASE(SerializableEnumUnknownException)
{
    std::string var("example3");
    BOOST_CHECK_THROW(ExampleEnum e1(var), std::runtime_error);

    std::string var2("");
    BOOST_CHECK_THROW(ExampleEnum e2(var), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(SerializableEnumComparators) {
    ExampleEnum e1(ExampleEnum::Example1);
    ExampleEnum e2(ExampleEnum::Example2);

    BOOST_CHECK(e1 == e1);
    BOOST_CHECK(e1 == ExampleEnum::Example1);
    BOOST_CHECK(! (e1 == e2));
    BOOST_CHECK(e1 < e2);
    BOOST_CHECK(e2 < ExampleEnum::EXAMPLE3);
}

BOOST_AUTO_TEST_CASE(SerializableEnumAssignmentOp) {
    ExampleEnum e(ExampleEnum::Example1);
    e = ExampleEnum::Example2;
    BOOST_CHECK(e == ExampleEnum::Example2);
}

BOOST_AUTO_TEST_CASE(SerializableEnumIterator)
{
    auto it = ExampleEnum::begin();

    ++it;
    BOOST_CHECK((*it).getValue() == ExampleEnum::Example2);

    ++it;
    ++it;
    BOOST_CHECK(it == ExampleEnum::end());
}


BOOST_AUTO_TEST_SUITE_END()
