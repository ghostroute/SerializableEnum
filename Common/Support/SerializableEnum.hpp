///////////////////////////////////////////////////////////
// SerializableEnum.hpp
//
// Copyright 2016 Cognotekt GmbH
// Authors: Philipp Kaluza <philipp.kaluza@cognotekt.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created by pkaluza on 2016-01-28
//
///////////////////////////////////////////////////////////

#include <map>
#include <unordered_map>
#include <string>

#ifdef HAVE_VARIANTASPECT
#include <Common/Support/FromVariantAspect.hpp>
#include <Common/Support/ToVariantAspect.hpp>
#endif

#ifndef _SERIALIZABLEENUM_HPP
#define _SERIALIZABLEENUM_HPP

namespace cognotekt {
namespace Common {

using register_enum_func = void (*)(int, const std::string&);

/** A helper class for "class enum"s. Allows look up for strings.
 */
template <typename EnumStruct, typename initer = EnumStruct, typename ComplexRepresentation = std::string>
class SerializableEnum : public EnumStruct
#ifdef HAVE_VARIANTASPECT
                       , public cognotekt::Common::FromVariantAspect
                       , public cognotekt::Common::ToVariantAspect
#endif
{
/* * * first part: class methods * * */
protected:
    using RawEnum = typename EnumStruct::_raw;
    using SE_type = SerializableEnum<EnumStruct, initer, ComplexRepresentation>;

private:

    // Internal hashmaps for forward and backward lookups:
    using forward_map_t  = std::map<RawEnum, ComplexRepresentation>;
    using backward_map_t = std::unordered_map<ComplexRepresentation, RawEnum>;

    class SEiterator
    {
    private:
        using internaltype = typename forward_map_t::const_iterator;

        internaltype m_iter;

    public:
        SEiterator(const internaltype& it) : m_iter(it) { }

        SEiterator& operator++() {
                m_iter++;
                return *this;
        };

        bool operator==(const SEiterator& other) {
            return m_iter == other.m_iter;
        }

        bool operator!=(const SEiterator& other) {
            return m_iter != other.m_iter;
        }

        SE_type operator*() const
        {
            return m_iter->first;
        }
    };

    static forward_map_t& fwdmap(){
        static forward_map_t sm_fwdmap;
        return sm_fwdmap;
    }

    static backward_map_t& bkwdmap(){
        static backward_map_t sm_bkwdmap;
        return sm_bkwdmap;
    }

    static void assert_initialized() {
        if (!initialized(false)){
            register_enum_func f = registerSE;
            initer::register_enums(f);
        }
        initialized(true);
    }

    static void registerSE(int id, const ComplexRepresentation& str) {
        ComplexRepresentation s = str;
        fwdmap().insert(
                std::make_pair((RawEnum)id, s));
        bkwdmap().insert(
                std::make_pair(s, (RawEnum)id));
    }

protected:
    static bool initialized(bool newly) {
        static bool sm_initialized = false;
        if (newly) {
            sm_initialized = true;
        }
        return sm_initialized;
    }

    static void registerSE(const RawEnum id, const ComplexRepresentation& str) {
        registerSE((int)id, str);
    }

public:
    static const ComplexRepresentation&
    toString(RawEnum val) {
        assert_initialized();
        // TODO: possibly fail hard on illegal enum values
        return fwdmap()[val];
    }

    static RawEnum
    fromString(const ComplexRepresentation& val) {
        assert_initialized();

        auto finder = bkwdmap().find(val);
        if (finder == bkwdmap().end()) {
            throw std::runtime_error("No corresponding enum for value " + val + " found");
        }
        return (finder->second);
    }

    /** This is not necessary for normal work, but allows the test suite to check
     *  the init-before-first-use code.
     */
    static bool initialized() {
        return initialized(false);
    }

    static SEiterator begin() {
        return SEiterator(fwdmap().begin());
    }

    static SEiterator end() {
        return SEiterator(fwdmap().end());
    }

/* * * second part : be a wrapper around the enum type * * */
protected:
    RawEnum m_value;

public:
    SerializableEnum(RawEnum val): m_value(val) {};

    SerializableEnum(const std::string& str):
            m_value(fromString(str))
    {};

#ifdef HAVE_LIBVARIANT
    // Compile in libvariant-compatible constructor / accessors where
    // available

    /** Constructor for creating a SerializableEnum from a Variant */
    SerializableEnum(const libvariant::Variant& varnt):
            m_value(fromString(varnt.AsString()))
    {};

    virtual libvariant::Variant getVariant() const
#ifdef HAVE_VARIANTASPECT
            override
#endif
    {
        return libvariant::Variant(toString(m_value));
    }
#endif

    RawEnum getValue() const {
        return m_value;
    }

    /** Implicit conversion to enum type. */
    operator RawEnum() const {
        return m_value;
    }

    const std::string& toString() const {
        return toString(m_value);
    }

    /** Equality operator.
     *  @param other enum to compare to
     *  @return true if equal; false otherwise
     */
    virtual bool operator==(const SE_type& other) const
    {
        return other.m_value == m_value;
    }

    /** Equality operator for comparision to raw enum
     *  @param other raw enum to compare to
     *  @return true if equal; false otherwise
     */
    virtual bool operator==(RawEnum other) const
    {
        return m_value == other;
    }

    /** Less-than operator.
     *  @param other enum to compare to
     *  @return true if less than "other"; false otherwise
     */
    virtual bool operator<(const SE_type& other) const
    {
        return m_value < other.m_value;
    }

    /** Less-than operator.
     *  @param other raw enum to compare to
     *  @return true if less than "other"; false otherwise
     */
    virtual bool operator<(RawEnum other) const
    {
        return m_value < other;
    }

    /** Assignment operator.
     *  @param rawval raw enum to assign to this instance
     */
    virtual void operator=(RawEnum rawval)
    {
        m_value = rawval;
    }

};

} // namespace Common
} // namespace cognotekt

#endif // _SERIALIZABLEENUM_HPP
