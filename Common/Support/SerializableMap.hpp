///////////////////////////////////////////////////////////
// SerializableMap.hpp
//
// (C) 2016 Cognotekt GmbH
// Authors: Daniel Beck <daniel.beck@cognotekt.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created by Daniel Beck on 22.05.16.
//
///////////////////////////////////////////////////////////

#pragma once

#include <Common/Support/FromVariantAspect.hpp>
#include <Common/Support/ToVariantAspect.hpp>
#include <map>

namespace cognotekt {
namespace Common {

template <typename FieldNamesProvider, typename Key, typename Value>
class SerializableMap
        : public std::map<Key, Value>
        , public FromVariantAspect
        , public ToVariantAspect
{
    using FNP = FieldNamesProvider;

  public:
    using Map = std::map<Key, Value>;

    SerializableMap() {}
    explicit SerializableMap(const Map& m) : Map(m) {}
    explicit SerializableMap(const libvariant::Variant& variant)
            : FromVariantAspect(variant)
    {
        if (!variant.IsList())
            throw DeserializationError("SerializableMap", variant["ruleId"].AsString(), "not a list");

        for (auto& elem : variant.AsList())
        {
            try
            {
                Key key = elem.Get(FNP::key).template As<Key>();
                Value value = elem.Get(FNP::value).template As<Value>();
                (*this)[key] = value;
            }
            catch (libvariant::KeyError& e)
            {
                throw DeserializationError("SerializableMap", variant["ruleId"].AsString(), e.what());
            }
        }
    }

    virtual ~SerializableMap() {}

    libvariant::Variant getVariant() const override
    {
        using namespace libvariant;

        Variant variant(VariantDefines::ListType);

        for (auto& pair : (*this))
        {
            Variant elem(VariantDefines::MapType);
            elem.Set(FNP::key, pair.first);
            elem.Set(FNP::value, pair.second);
            variant.Append(elem);
        }

        return variant;
    }
};

} // namespace Common
} // namespace cognotekt
